import csv
from random import randint
import tkinter as tk
from tkinter import ttk


class Main(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.init_main()

    def init_main(self):
        global result
        toolbar = tk.Frame(bg='#d7d8e0', bd=2)
        toolbar.pack(side=tk.TOP, fill=tk.X)

        self.add_img = tk.PhotoImage(file="like.png")
        btn_open_dialog = tk.Button(toolbar, command=self.open_dialog, bg='#d7d8e0', bd=0,
                                    compound=tk.TOP, image=self.add_img)
        btn_open_dialog.pack(side=tk.TOP)

        self.tree = ttk.Treeview(self, columns=('showed', 'won'),
                                 height=15, show='headings')

        self.tree.column("showed", width=250, anchor=tk.CENTER)
        self.tree.column("won", width=250, anchor=tk.CENTER)

        self.tree.heading("showed", text='Cards showed')
        self.tree.heading("won", text='Correct answers')

        [self.tree.delete(i) for i in self.tree.get_children()]
        self.tree.insert('', 'end', values=result)

        self.tree.pack()

    def open_dialog(self):
        global result
        Child()
        [self.tree.delete(i) for i in self.tree.get_children()]
        self.tree.insert('', 'end', values=result)


class Child(tk.Toplevel):
    def __init__(self):
        super().__init__(root)
        self.init_child()

    def init_child(self):
        self.wordrow = get_random_row(dictionary)
        self.title('Word Card')
        self.geometry('400x200+350+250')
        self.resizable(False, False)

        label_word = tk.Label(self, text='Word:')
        label_word.place(x=50, y=50)

        label_get_word = tk.Label(self, text=self.get_word())
        label_get_word.place(x=200, y=50)

        label_answer = tk.Label(self, text='Correct answer:')
        label_answer.place(x=50, y=80)

        self.label_correct_answer = tk.Label(self, text='')
        self.label_correct_answer.place(x=200, y=80)

        label_ans = tk.Label(self, text='Your answer:')
        label_ans.place(x=50, y=110)

        self.entry_ans = ttk.Entry(self)
        self.entry_ans.place(x=200, y=110)

        btn_cancel = ttk.Button(self, text='Close', command=self.destroy)
        btn_cancel.place(x=300, y=170)

        self.btn_ok = ttk.Button(self, text='Answer', command=self.answer)
        self.btn_ok.place(x=220, y=170)

        self.grab_set()
        self.focus_set()

    def get_word(self):
        return self.wordrow[0]

    def answer(self):
        global result
        self.label_correct_answer["text"] = self.wordrow[1]
        self.btn_ok.destroy()
        result[0] +=1
        ans = self.entry_ans.get()
        if eval_answer(ans,self.wordrow[1]):

            result[1] +=1
            self.label_correct_answer["text"] += '!!!Correct!!!'
        
        


def get_random_row(dictionary):
    num = randint(0, max(dictionary))
    return dictionary[num]

dictionary = {}
i = int(0)

with open('dict.csv', 'r') as inp:
    data = csv.reader(inp, delimiter=";")
    for row in data:
        i+=1
        dictionary[i] = row

def eval_answer(ans,correct):
    if ans in correct:
        return True
    else:
        return False

if __name__ == "__main__":
    result = [int(0), int(0)]
    root = tk.Tk()
    app = Main(root)
    app.pack()
    root.title("WordCards")
    root.geometry("500x350+300+200")
    root.resizable(False, False)
    root.mainloop()
